<?php

namespace Wp;

use Ngearing\Wp\Loader;

class Frontend {



	public function __construct() {
		$this->loader = new Loader();

		$this->register_frontend_hooks();
	}

	private function register_frontend_hooks() {
		$this->loader->add_action( 'beforeend_main_nav', $this, 'main_nav_extra_content' );
		$this->loader->add_action( 'afterbegin_site_main', $this, 'home_feature_image' );
		$this->loader->add_action( 'beforeend_site_main', $this, 'home_latest_projects' );
		$this->loader->add_action( 'beforeend_site_main', $this, 'home_upcoming_projects' );

		$this->loader->add_filter( 'get_the_archive_title', $this, 'archive_title' );
	}

	public function main_nav_extra_content() {
		include get_theme_file_path( 'templates/section-main-menu.php' );
	}

	public function home_feature_image() {
		if ( ! is_front_page() ) {
			return;
		}

		include get_theme_file_path( 'templates/section-featured-project.php' );

		// Open wrapper for middle columns.
		echo "<div class='container'>";
	}

	public function home_latest_projects() {
		include get_theme_file_path( 'templates/section-latest-projects.php' );
		echo '</div>';
	}

	public function home_upcoming_projects() {
		include get_theme_file_path( 'templates/section-upcoming-projects.php' );
	}

	public function archive_title( $title ) {
		if ( is_post_type_archive() || is_tax() ) {
			$title = get_post_type_object( get_post_type( get_the_id() ) )->label;
		} elseif ( is_singular() ) {
			$title = get_post_type_object( get_post_type( get_the_id() ) )->label;
		}

		return $title;
	}

	public function run() {
		$this->loader->run();
	}
}
