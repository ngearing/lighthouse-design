<?php

/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

get_header();
?>

	<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			$taxonomy = 'project-category';

			include get_theme_file_path( 'templates/nav-project-category.php' );

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				include get_theme_file_path( 'templates/post-tile.php' );

		endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'templates/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();
