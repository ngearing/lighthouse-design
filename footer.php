<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wbs
 */

?>

	<?php do_action( 'beforebegin_footer' ); ?>

	<footer id="colophon" class="site-footer">

		<?php do_action( 'afterbegin_footer' ); ?>

		<div class="footer-contact">
			<p><b>If you would like to work with us on your next <br>
				project, you can speak to one of us via <br>
				the following methods:</b></p>

			<hr>

			<?php
			printf(
				'<p><a href="mailto:%s">%1$s</a></p>
				<p><a href="tel:%s">%2$s</a></p>',
				get_field( 'email', 'options' ),
				get_field( 'phone', 'options' )
			);
			?>
		</div>

		<div class="site-info">

			<?php require get_theme_file_path( 'templates/section-social-links.php' ); ?>

			<p class="site-copy">
			<?php
			printf(
				'&copy; %s %d - %s',
				get_bloginfo(),
				date( 'Y' ),
				get_bloginfo( 'description' )
			);
			?>
			</p>
			<p class="site-address"><?php echo get_field( 'address', 'options' ); ?></p>
			<p class="site-by">Website by <a href="https://greengraphics.com.au" target="_blank">Greengraphics</a></p>
		</div><!-- .site-info -->

		<?php do_action( 'beforeend_footer' ); ?>

	</footer><!-- #colophon -->

	<?php do_action( 'afterend_footer' ); ?>

	<?php wp_footer(); ?>
</div><!-- #page -->

</body>
</html>
