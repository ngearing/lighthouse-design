<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

get_header();
?>

	<?php do_action( 'beforebegin_site_main' ); ?>

	<main id="main" class="site-main">

		<?php do_action( 'afterbegin_site_main' ); ?>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'templates/content', 'home' );

		endwhile; // End of the loop.
		?>

		<?php do_action( 'beforeend_site_main' ); ?>

	</main><!-- #main -->

	<?php do_action( 'afterend_site_main' ); ?>

<?php
get_footer();
