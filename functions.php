<?php

use Wp\Frontend;
use Ngearing\Wp\PostType;
use Wp\PostTax;

/**
 * wbs functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wbs
 */

require 'vendor/autoload.php';

new Wp\Cleanup();
new Wp\Theme();

if ( class_exists( 'ACF' ) ) {
	new Wp\Acf();
}

new PostType(
	'Project', [
		'menu_icon' => 'dashicons-admin-multisite',
		'supports'  => [ 'title', 'editor', 'revisions', 'thumbnail' ],
	]
);

new PostTax( 'project-category', 'project' );

$frontend = new Wp\Frontend();
$frontend->run();
