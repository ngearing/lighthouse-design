<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

get_header();
?>

	<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'templates/content', 'page' );

		endwhile; // End of the loop.
		?>

		<?php
		$fields = get_fields();
		if ( $fields ) :
			?>
			<div class="extra-content">
			<?php
			foreach ( $fields as $key => $field ) :

				if ( file_exists( get_theme_file_path( "templates/field-$key.php" ) ) ) {
					include get_theme_file_path( "templates/field-$key.php" );
				} elseif ( ! is_array( $field ) ) {
					include get_theme_file_path( 'templates/field-text.php' );
				}

			endforeach;
			?>
			</div>
			<?php
		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();
