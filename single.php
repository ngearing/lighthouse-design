<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wbs
 */

get_header();
?>

	<main id="main" class="site-main">

		<header class="page-header">
			<?php
			the_archive_title( '<h2 class="page-title"><a href="' . get_post_type_archive_link( get_post_type() ) . '">', '</a></h2>' );
			?>
		</header><!-- .page-header -->

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'templates/content', 'project' );

		endwhile; // End of the loop.
		?>

		<?php require get_theme_file_path( 'templates/field-gallery.php' ); ?>

		<?php the_post_navigation(); ?>

	</main><!-- #main -->

<?php
get_footer();
