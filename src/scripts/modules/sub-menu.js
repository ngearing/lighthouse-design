;(function() {
    "use strict"

    const toggles = document.querySelectorAll(".sub-menu-toggle")
    if (!toggles) {
        return
    }

    for (let index = 0; index < toggles.length; index++) {
        let menuToggle = toggles[index]
        menuToggle.addEventListener("click", toggleSubMenu)
    }

    function toggleSubMenu(e) {
        let menu = this.parentNode
        menu.classList.toggle("sub-menu-open")
    }
})()
