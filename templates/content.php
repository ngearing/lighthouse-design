<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>

	<?php
	if ( is_singular() ) :
		echo wp_get_attachment_image(
			get_post_thumbnail_id( get_the_ID() ),
			'post-thumbnail',
			false,
			[ 'class' => 'post-thumbnail' ]
		);
else :
	?>
<a href="<?php the_permalink(); ?>">
			<?php
			echo wp_get_attachment_image(
				get_post_thumbnail_id( get_the_ID() ),
				'post-tile',
				false,
				[ 'class' => 'post-thumbnail' ]
			);
			?>
	</a>
			<?php
		endif;

?>

	<header class="post-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="post-title">', '</h1>' );
	else :
		the_title( '<h2 class="post-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
	endif;
	?>
	</header><!-- .post-header -->

	<div class="post-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wbs' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="post-links">' . esc_html__( 'Pages:', 'wbs' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .post-content -->

</article><!-- #post-<?php the_ID(); ?> -->
