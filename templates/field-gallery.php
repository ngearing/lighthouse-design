<?php
$gallery = get_field( 'gallery' );
if ( ! $gallery ) {
	return;
}
?>

<section class="gallery">
	<?php foreach ( $gallery as $image ) : ?>

		<div class="gallery-item">
			<?php echo wp_get_attachment_image( $image['ID'], 'medium' ); ?>
		</div>

	<?php endforeach; ?>
</section>
