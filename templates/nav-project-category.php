<?php
$current_term = get_queried_object_id();
?>

<nav class="project-categories">
	<ul class='menu'>

		<li class="term all">
			<a href="<?php echo get_post_type_archive_link( 'project' ); ?>">All Projects</a>
		</li>
		<?php
		$taxonomy = 'project-category';
		$terms    = get_terms( $taxonomy );

		foreach ( $terms as $term ) {
			printf(
				'<li class="term %s"><a href="%s">%s</a></li>',
				$current_term === $term->term_id ? 'current' : '',
				get_term_link( $term->term_id, $taxonomy ),
				$term->name
			);
		}

		?>
	</ul>
</nav>
