<div class="page-thumbnail">
	<?php echo wp_get_attachment_image( get_post_thumbnail_id( get_the_id() ), isset( $size ) ? $size : 'large' ); ?>
</div>
