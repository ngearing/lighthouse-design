<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wbs
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>

<a href="<?php the_permalink(); ?>">
	<?php
	echo wp_get_attachment_image(
		get_post_thumbnail_id( get_the_ID() ),
		'post-tile',
		false,
		[ 'class' => 'post-thumbnail' ]
	);
	?>
	</a>

	<header class="post-header">

		<?php if ( isset( $date ) ) : ?>
		<p class="date"><?php echo get_the_date(); ?></p>
	<?php endif; ?>

		<?php
		the_title( '<h2 class="post-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		?>

	<?php if ( isset( $taxonomy ) ) : ?>
		<p class="terms"><?php echo get_the_term_list( get_the_id(), $taxonomy, '', ', ' ); ?></p>
	<?php endif; ?>

	</header><!-- .post-header -->

</article><!-- #post-<?php the_ID(); ?> -->
