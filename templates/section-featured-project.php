<section class="featured-project">
<?php
$project = new WP_Query(
	[
		'post_type'      => 'project',
		'posts_per_page' => 1,
		'orderby'        => 'rand',
	]
);

if ( $project->have_posts() ) :
	while ( $project->have_posts() ) :
		$project->the_post();
		?>

		<div class="page-thumbnail">
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<?php echo wp_get_attachment_image( get_post_thumbnail_id( get_the_id() ), 'banner' ); ?>
			</a>
		</div>

		<header class="post-header">
			<?php
			the_title( '<h2 class="post-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			?>
		</header><!-- .post-header -->

		<?php
	endwhile;
endif;
?>
</section>
