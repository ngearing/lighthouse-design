<section class="latest-projects">
	<header>
		<h2>Latest Projects</h2>
	</header>
	<?php
	$projects = new WP_Query(
		[
			'post_type'      => 'project',
			'posts_per_page' => 2,
			'tax_query'      => [
				[
					'taxonomy' => 'project-category',
					'terms'    => [ 'upcoming-projects' ],
					'field'    => 'slug',
					'operator' => 'NOT IN',
				],
			],
		]
	);

	$taxonomy = 'category';

	if ( $projects->have_posts() ) :
		while ( $projects->have_posts() ) :
			$projects->the_post();
			include get_theme_file_path( 'templates/post-tile.php' );
	endwhile;
endif;
	?>

	<footer>
		<a href="<?php echo get_post_type_archive_link( 'project' ); ?>">View All Projects</a>
	</footer>

</section>
