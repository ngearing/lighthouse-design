<div class="menu-content">

	<p class="site-title"><?php bloginfo(); ?></p>

	<p class="site-address"><?php echo get_field( 'address', 'options' ); ?></p>

	<p class="site-phone"><a href="tel:<?php echo get_field( 'phone', 'options' ); ?>"><?php echo get_field( 'phone', 'options' ); ?></a></p>

	<p class="site-email"><a href="mailto:<?php echo get_field( 'email', 'options' ); ?>"><?php echo get_field( 'email', 'options' ); ?></a></p>

	<?php require get_theme_file_path( 'templates/section-social-links.php' ); ?>

</div>
