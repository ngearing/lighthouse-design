<?php
$links = get_field( 'social_links', 'options' );
if ( ! $links ) {
	return;
}
?>

<div class="social-links">
	<?php foreach ( $links as $value ) : ?>

		<?php
		$name = explode( '.', parse_url( str_replace( 'www.', '', $value['link'] ) )['host'] )[0];
		?>

		<a href="<?php echo $value['link']; ?>" class="social-link" target="_blank">
			<span class="sr-only"><?php echo $value; ?></span>
			<i class="fab fa-<?php echo $name; ?>"></i>
		</a>

	<?php endforeach; ?>
</div>
