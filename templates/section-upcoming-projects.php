<section class="upcoming-projects">
	<header>
		<h2>Upcoming Projects</h2>
	</header>
	<?php
	$projects = new WP_Query(
		[
			'post_type'      => 'project',
			'posts_per_page' => 3,
			'tax_query'      => [
				[
					'taxonomy' => 'project-category',
					'terms'    => [ 'upcoming-projects' ],
					'field'    => 'slug',
					'operator' => 'IN',
				],
			],
		]
	);

	$date = true;

	if ( $projects->have_posts() ) :
		while ( $projects->have_posts() ) :
			$projects->the_post();

			include get_theme_file_path( 'templates/post-tile.php' );

	endwhile;
endif;
	?>
</section>
